# Configuration de Kubernetes à l'aide d'Ansible et de Vagrant

Vagrant est un outil qui va nous permettre de créer facilement un environnement virtuel et il élimine les pièges qui provoquent le phénomène des travaux sur ma machine. Il peut être utilisé avec plusieurs fournisseurs tels que Oracle VirtualBox, VMware, Docker, etc. Cela nous permet de créer un environnement jetable en utilisant des fichiers de configuration.

Ansible est un moteur d'automatisation d'infrastructure qui automatise la gestion de la configuration logicielle. Il est sans agent et nous permet d'utiliser des clés SSH pour nous connecter à des machines distantes. Les playbooks Ansible sont écrits en yaml et offrent une gestion des stocks dans de simples fichiers texte.

## Conditions préalables

Vagrant doit être installé sur votre machine. Les binaires d'installation peuvent être trouvés ici .
Oracle VirtualBox peut être utilisé comme fournisseur Vagrant ou utiliser des fournisseurs similaires comme décrit dans la documentation officielle de Vagrant .
Ansible doit être installé sur votre machine. Reportez-vous au guide d'installation d'Ansible pour une installation spécifique à la plate-forme.

## Commande
```bash
vagrant up --provider=virtualbox
#Accessing master
vagrant ssh k8s-master
vagrant@k8s-master:~$ kubectl get nodes
NAME         STATUS   ROLES    AGE     VERSION
k8s-master   Ready    master   18m     v1.13.3
node-1       Ready    <none>   12m     v1.13.3
node-2       Ready    <none>   6m22s   v1.13.3

# Accessing nodes
vagrant ssh node-1
vagrant ssh node-2

vagrant halt
```